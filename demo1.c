#include <ncurses.h>
#include <unistd.h>

#define DELAY 9000

int kbhit(void);

int main(int argc, char *argv[]) {
    int x=0, y=0, y2=0, x2=0;
    int max_y=0, max_x = 0;
    int next_x = 0;
    int next_y = 0;
    int direction_x = 1;
    int direction_y = 1;
    int keepIn = 1;

    initscr(); //Init ncurses screen.
    noecho(); //Disable echo from keyboard.
    curs_set(FALSE); //Control cursor state to make it invisible.
    cbreak(); //Make characters read one by one.
    nodelay(stdscr, TRUE); //Makes getch() be non-blocking. Usefull for replicate Borland's kbhit() function.
    scrollok(stdscr, TRUE); //Some cursor control.

    while(keepIn == 1)
    {
        if(kbhit) //Reads a character without block.
        {
            char out;
            out = getch();
            if(out == 'q' || out == 'Q')
                keepIn = 0;
        }
        
        getmaxyx(stdscr, max_y, max_x); //Obtains the terminal dimensions.
        
        clear(); //Clear the screen.

        mvprintw(y, x, "O"); //Adds move() and printw() funtionality.

        usleep(DELAY);
        
        next_x = x + direction_x;
        next_y = y + direction_y;
        
        y2 = y;
        x2 = x;
        
        mvprintw(max_y-1, 0, "Press Q to quit.");
        mvprintw(max_y-3, 0, "next_y = %d\tnext_x=%d\tdirection_y=%d\tdirection_x=%d", next_y, next_x, direction_y,  direction_x);
        mvprintw(max_y-2, 0, "y = %d\tx = %d\t\tmax_y = %d\t\tmax_x = %d", y2, x2, max_y-4, max_x-1);
        refresh();
        usleep(DELAY);

        
        if(next_x >= max_x || next_x < 0)
        {
            direction_x*= -1;
            
            if(next_y >= (max_y-3) || next_y < 0)
            {
                direction_y *= -1;
            }
            
            else
            {
                y += direction_y;
            }
            
        }
        
        else
        {
            x+= direction_x;
        }
        
    }

    endwin(); //Exit from ncurses mode.
}

int kbhit(void)
{
    //Returns ERR when nothing is in the buffer.
    
    int ch = getch();

    if (ch != ERR) {
        ungetch(ch); //Push ch to the buffer so the next getch could read it again.
        return 1;
    } else {
        return 0;
    }
}
