Use of ncurses library for conio replacement, especially in kbhit() and getch() functions.
An example of the program running: https://asciinema.org/a/yaGNqBGGpktBotbr94GurnFEd
